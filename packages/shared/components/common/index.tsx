import Image from "next/image";
import classNames from "classnames";
import Link from "next/link";
import React from "react";

const Headers: React.FC = () => {
  return (
    <div className="pb-[51px]">
      <header
        className={classNames([
          "fixed top-0 z-10 bg-[#F05A94] max-h-14 w-full",
        ])}
      >
        <div className="container flex h-auto items-center gap-5">
          <div className="col-span-2">
            <Link href="/">
              <div className="relative h-9 lg:h-10">
                <Image
                  alt="logo vuivui"
                  width={180}
                  height={40}
                  className="float-right h-9 w-auto cursor-pointer lg:h-10"
                  src="/static/svg/vuivui-logo-ef5191.svg"
                />
              </div>
            </Link>
          </div>
          <div className="relative" style={{ flex: 1 }}>
            <div
              className={`flex items-center bg-white h-11 rounded-md ${
                true && "border-black"
              }`}
            >
              <div
                className={`flex items-center cursor-pointer bg-[#FDEBF2] p-1 ml-1 rounded-l-[4px] whitespace-nowrap`}
                // tabIndex={0}
                // role='menu'
                // onMouseEnter={onOpen}
                // onMouseLeave={onClose}
              >
                <Image
                  width={16}
                  height={16}
                  loading="lazy"
                  className="h-auto w-4"
                  src="/static/svg/list-950135.svg"
                />
                <span className="px-1 text-base font-normal not-italic leading-normal tracking-[0.04px] text-[#950135]">
                  Danh mục
                </span>
                <Image
                  loading="lazy"
                  width={12}
                  height={12}
                  className="h-auto w-3"
                  src="/static/svg/chevron-down-bold-950135.svg"
                  alt="chevron vuivui"
                />
              </div>
              {/* <Search isFocus={isFocus} setIsFocus={setIsFocus} /> */}
            </div>
          </div>
          <div className="relative">
            <div className="flex flex-row gap-3 pt-[8px]">
              <Link href={"/live-stream"}>
                <div className="flex cursor-pointer flex-col">
                  <div className="relative mx-auto h-5">
                    <Image
                      width={15}
                      height={15}
                      loading="lazy"
                      className="h-5 w-auto"
                      src="/static/svg/live-stream-ffffff.svg"
                      alt="livestream vuivui"
                    />
                    {/* <div className='not-italic font-bold text-[11px] text-[#333333] tracking-[0.96px] font-sfpro_bold bg-[#FEE800] px-[6px] py-0 rounded-[15px] absolute translate-x-1/2 -translate-y-1/2 top-1/2 -right-1/3'>
											0
										</div> */}
                  </div>
                  <span className="text-base font-normal not-italic leading-normal tracking-[0.04px] text-white">
                    Livestream
                  </span>
                </div>
              </Link>

              <div
                className="flex cursor-pointer flex-col"
                // role='menubar'
                // tabIndex={0}
                // onClick={() => handleListChat()}
                // onKeyPress={() => handleListChat()}
              >
                <div className="relative mx-auto h-5">
                  <Image
                    width={15}
                    height={15}
                    loading="lazy"
                    className="h-5 w-auto"
                    src="/static/svg/chat-ffffff.svg"
                  />
                  {/* <div className='absolute top-1 -right-1/3 translate-x-1/2 -translate-y-1/2 rounded-[15px] bg-[#FEE800] px-[6px] py-0 font-sfpro_bold text-[11px] font-bold not-italic tracking-[0.96px] text-[#333333]'>
										{listConversation &&
											listConversation?.filter(
												(item) =>
													item.participate_user_id !== item.last_message?.SenderId &&
													item.last_message?.Status !== CHAT_STATUS.SEEN,
											).length > 0 &&
											listConversation?.filter(
												(item) =>
													item.participate_user_id !== item.last_message?.SenderId &&
													item.last_message?.Status !== CHAT_STATUS.SEEN,
											).length}
									</div> */}
                </div>
                <span className="text-base font-normal not-italic leading-normal tracking-[0.04px] text-white">
                  Tin nhắn
                </span>
              </div>
              <Link href={"/ca-nhan/don-hang"}>
                <a className="flex cursor-pointer flex-col">
                  <div className="relative mx-auto h-5">
                    <Image
                      width={15}
                      height={15}
                      loading="lazy"
                      className="h-5 w-auto"
                      src="/static/svg/person-ffffff.svg"
                    />
                  </div>
                  <span className="text-base font-normal not-italic leading-normal tracking-[0.04px] text-white">
                    Cá nhân
                  </span>
                </a>
              </Link>
              <Link href={"/gio-hang"}>
                <a className="flex cursor-pointer flex-col">
                  <div className="relative mx-auto h-5">
                    <Image
                      width={15}
                      height={15}
                      loading="lazy"
                      className="h-5 w-auto"
                      src="/static/svg/cart-ffffff.svg"
                    />
                    {/* {totalItemCart.total > 0 && (
											<div className='absolute top-1/2 -right-1/3 translate-x-1/2 -translate-y-1/2 rounded-[15px] bg-[#FEE800] px-[6px] py-0 font-sfpro_bold text-[11px] font-bold not-italic tracking-[0.96px] text-[#333333]'>
												{totalItemCart.total}
											</div>
										)} */}
                  </div>
                  <span className="text-base font-normal not-italic leading-normal tracking-[0.04px] text-white">
                    Giỏ hàng
                  </span>
                </a>
              </Link>
            </div>
            {/* <div className='flex justify-center'>
							<div
								className={`absolute top-[52px] items-center h-auto bg-white ${
									isOpenChat ? 'opacity-100 z-40' : 'opacity-0 hidden'
								} container`}
							>
								<ListChat
									setIsOpenChat={setIsOpenChat}
									listConversation={listConversation}
									handleRoomChat={handleRoomChat}
								/>
							</div>
						</div> */}
          </div>
        </div>
        {/* <div className='flex justify-center'>
					<div
						className={classNames([
							isOpenCatalog ? 'opacity-100 z-40' : 'opacity-0 hidden',
							'transition-all duration-500',
							'absolute items-center h-auto bg-gray-500 container',
						])}
					>
						<Catalog
							onOpen={onOpen}
							onClose={onClose}
							userAuth={userAuth}
							isOpenCatalog={isOpenCatalog}
						/>
					</div>
				</div> */}
      </header>
    </div>
  );
};

export default Headers;
