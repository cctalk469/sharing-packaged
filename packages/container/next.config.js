const withTM = require("next-transpile-modules")(["shared"]);

const moduleExports = withTM({
  webpack: (config) => {
    config.module.rules.push({
      test: /\.+(js|jsx|mjs|ts|tsx)$/,
      use: {
        loader: "babel-loader",
        options: {
          presets: ["next/babel"],
        },
      },
    });
    return config;
  },
  async rewrites() {
    return [
      {
        source: "/:path*",
        destination: `/:path*`,
      },
      {
        source: "/customer",
        destination: `${process.env.NEXT_PUBLIC_DOMAIN_URL}/customer`,
      },
      {
        source: "/customer/:path*",
        destination: `${process.env.NEXT_PUBLIC_DOMAIN_URL}/customer/:path*`,
      },
    ];
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  experimental: {
    outputStandalone: true,
  },
  externalDir: true,
  reactStrictMode: true,
  swcMinify: process.env.NODE_ENV !== "production",
  compress: true,
  pageExtensions: ["mdx", "md", "jsx", "js", "tsx", "ts"],
});

module.exports = moduleExports;
